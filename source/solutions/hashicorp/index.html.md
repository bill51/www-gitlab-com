---
layout: solutions
title: GitLab on HashiCorp
description: "A complete DevOps platform to build, test, secure and deploy on HashiCorp Terraform"
suppress_header: true
---

***
{:.header-start}

# GitLab on HashiCorp
{:.header-left}

A complete DevOps platform to build, test, secure and deploy on HashiCorp Terraform
{:.header-left}

[Talk to an expert](/sales/){: .btn .cta-btn .orange}
{:.header-left}


***
{:.header-end}

> From issue tracking and source code management to CI/CD and monitoring, go from idea to production faster with GitLab on HashiCorp.

> GitLab is a complete DevOps platform, delivered as a single application with bring-your-own-infrastructure flexibility. GitLab and HashiCorp have partnered to enable you to combine application delivery workflow with infrastructure delivery workflow.

> GitLab and HashiCorp Terraform offer a number of integrations to make it easy to manage infrastructure as a part of an organization's CI/CD workflow. GitLab can be used as the VCS provider for both Terraform Cloud and Enterprise and the GitLab provider can be used to manage groups and resources.You can deploy GitLab anywhere from the cloud to on-premise and use GitLab to deploy your software anywhere. By running GitLab on HashiCorp and using GitLab to deploy your software to HashiCorp, you get a complete DevOps platform running and deploying to HashiCorp cloud infrastructure.

---

## Joint HashiCorp and GitLab Benefits

GitLab collapses cycle times by driving higher efficiency across all stages of the software development lifecycle running on HashiCorp. 

* Projects delivered on-time and budget
    * Eliminate bottlenecks for agility, faster DevOps lifecycle, reduce re-work, reduce unpredictable spend
* Increase team productivity and velocity.
    * Attract, retain, and enable top talent, move engineers from integrations and maintenance to innovation, happy developers.
* Increase market share and revenue
    * Faster time to market, disrupt the competition; increased innovation; more expansive product roadmap.
* Increase customer satisfaction
    * Decrease security exposure, cleaner, and easier audits reduce disruptions.
{:.list-benefits}

---

## Joint Solution Capabilities with HashiCorp

- ![Terraform logo](/images/solutions/hashicorp/Terraform_PrimaryLogo_Black_RGB.svg)Terraform
  - HashiCorp Terraform is an open source tool for provisioning infrastructure as code. Users define infrastructure in HashiCorp Configuration Language (HCL) configuration files, Terraform reads those configurations, offers a speculative plan of what it will create, and then users confirm and apply those changes. Terraform keeps track of what infrastructure is provisioned in a state file. **[[Learn More]](https://www.terraform.io/)**
- ![Vault logo](/images/solutions/hashicorp/Vault_PrimaryLogo_Black_RGB.svg)Vault Integration
  - HashiCorp's Vault is paving the way for OSS Secrets Management and many GitLab customers are leveraging the solution today. Vault let’s you easily rotate secrets and can manage intermediate, temporary tokens used between different services. This ensures there are no long-term tokens lying around or commonly used. Vault minimizes GitLab's attack surface and protects against any unknown zero-day vulnerabilities in our Rails app today or those that allow a bad actor to access the Gitlab server by ensuring that GitLab does not hold any long term secrets. **[[Learn More]](https://www.vaultproject.io/)**
{:.list-capabilities}

---

## GitLab and HashiCorp Joint Solutions in Action
{:.no-color}

- [GitLab and HashiCorp: Providing application and infrastructure delivery workflows](https://about.gitlab.com/blog/2019/09/17/gitlab-hashicorp-terraform-vault-pt-1/)
- [How Wag! cut their release process from 40 minutes to just 6](https://about.gitlab.com/blog/2019/01/16/wag-labs-blog-post/)
- [GitLab and HashiCorp - A holistic guide to GitOps and the Cloud Operating Model](https://about.gitlab.com/webcast/gitlab-hashicorp-gitops/)
{:.list-resources}

---
